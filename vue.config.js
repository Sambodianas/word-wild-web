module.exports = {
	css: {
		loaderOptions: {
			sass: {
				data: '@import "~@/sass/main.scss"',
			},
		},
	},

  publicPath: process.env.NODE_ENV === 'production'
    ? '/word-wild-web/'
    : '/'
}
